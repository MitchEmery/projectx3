//
//  InterfaceController.swift
//  projectX3 WatchKit Extension
//
//  Created by Mitchell Emery on 2016-12-07.
//  Copyright © 2016 Mitchell Emery. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
   
    var session: WCSession!
    
    @IBOutlet var _lblResults: WKInterfaceLabel!

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        //check to see if its the first time or not
        if(context != nil){
            //check results
            if(context as! Bool){
                _lblResults.setText("Correct!")
            }else{
                _lblResults.setText("Incorrect")
            }
        }
        
        
        //activate the session
        if (WCSession.isSupported()) {
            session = WCSession.default()
            session.delegate = self;
            session.activate()
        }
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    

    //the action methods for only button on page
    //SHOULD BE NAMED GETSPORTSQUESTION
    @IBAction func moveToQuestionPage() {
        //ask for a quiz
        AskForQuiz(s: "Sports")
    }
    
    @IBAction func GetGeographyQuestion() {
        //ask for a quiz
        AskForQuiz(s: "Geography")
    }
    
    @IBAction func GetScienceQuestion() {
        //ask for a quiz
        
        AskForQuiz(s: "Science")
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error:Error?)
    {
        //called when the activation is completed
    }
    
    func AskForQuiz(s: String){
        
        
        
        //Send Message
        let messageToSend = ["Value":s]
        session.sendMessage(messageToSend, replyHandler: { replyMessage in
            //handle the reply
            _ = replyMessage["Value"] as? String
            //use dispatch_asynch to present immediately on screen
            
        }, errorHandler: {error in
            // catch any errors here
            print(error)
        })
        
    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void)
    {
        let quest = Question(title: (message["Question"] as? String)!, ans: (message["Answer"] as? String)!, wrong: [(message["Wrong1"] as? String)!, (message["Wrong2"] as? String!)!, (message["Wrong3"] as? String!)!])
        
        
        pushController(withName: "WatchQuestionPage", context: quest)
        
    }
}
