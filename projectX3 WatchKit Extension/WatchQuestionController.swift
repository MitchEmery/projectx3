//
//  WatchQuestionController.swift
//  projectX3
//
//  Created by Mitchell Emery on 2016-12-08.
//  Copyright © 2016 Mitchell Emery. All rights reserved.
//

import UIKit
import Foundation
import WatchKit

class WatchQuestionController: WKInterfaceController {

    //the correct answer variable
    var correctAnswer: Int!
    
    //UI Elements
    //The Label for displaying the question
    @IBOutlet var _lblQText: WKInterfaceLabel!
    //the buttons
    @IBOutlet var _btnAns1: WKInterfaceButton!
    @IBOutlet var _btnAns2: WKInterfaceButton!
    @IBOutlet var _btnAns3: WKInterfaceButton!
    @IBOutlet var _btnAns4: WKInterfaceButton!
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        //context is the objects retrieved from last view
        // Configure interface objects here.
        
        let q:Question = context as! Question
        
        _lblQText.setText(q.QTitle)
        
        //assign randomly a answer to a button
        
        let assigning = Int(arc4random_uniform(4) + 1)
        
        switch assigning {
        case 1:
            _btnAns1.setTitle(q.QAnswer)
            _btnAns2.setTitle(q.QWAnswer[2])
            _btnAns3.setTitle(q.QWAnswer[0])
            _btnAns4.setTitle(q.QWAnswer[1])
            //assign the truth var
            correctAnswer = 1
            break
        case 2:
            _btnAns1.setTitle(q.QWAnswer[2])
            _btnAns2.setTitle(q.QAnswer)
            _btnAns3.setTitle(q.QWAnswer[1])
            _btnAns4.setTitle(q.QWAnswer[0])
            //assign the truth var
            correctAnswer = 2
            break
        case 3:
            _btnAns1.setTitle(q.QWAnswer[0])
            _btnAns2.setTitle(q.QWAnswer[1])
            _btnAns3.setTitle(q.QAnswer)
            _btnAns4.setTitle(q.QWAnswer[2])
            //assign the truth var
            correctAnswer = 3
            break
        default:
            _btnAns1.setTitle(q.QWAnswer[1])
            _btnAns2.setTitle(q.QWAnswer[2])
            _btnAns3.setTitle(q.QWAnswer[0])
            _btnAns4.setTitle(q.QAnswer)
            //assign the truth var
            correctAnswer = 4
            break
        }
    }
    
    
    @IBAction func ButtonOnePressed() {
        if (correctAnswer == 1){
            pushController(withName: "MainPage", context: true)
        }else{
            pushController(withName: "MainPage", context: false)
        }
    }
    
    @IBAction func ButtonTwoPressed() {
        if(correctAnswer == 2){
            pushController(withName: "MainPage", context: true)
        }else{
            pushController(withName: "MainPage", context: false)
        }
        
    }
    
    @IBAction func ButtonThreePressed() {
        if(correctAnswer == 3){
            pushController(withName: "MainPage", context: true)
        }else{
            pushController(withName: "MainPage", context: false)
        }
        
    }
    
    @IBAction func ButtonFourPressed() {
        if(correctAnswer == 4){
            pushController(withName: "MainPage", context: true)
        }else{
            pushController(withName: "MainPage", context: false)
        }
    }

    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
}
