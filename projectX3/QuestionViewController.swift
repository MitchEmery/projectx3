//
//  QuestionViewController.swift
//  projectX3
//
//  Created by Mitchell Emery on 2016-12-07.
//  Copyright © 2016 Mitchell Emery. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {
    
    // all the UI Elements
    @IBOutlet weak var _QuestText: UILabel!
    @IBOutlet weak var _lblCategory: UILabel!
    @IBOutlet weak var _btnChoice1: UIButton!
    @IBOutlet weak var _btnChoice2: UIButton!
    @IBOutlet weak var _btnChoice3: UIButton!
    @IBOutlet weak var _btnChoice4: UIButton!
    @IBOutlet weak var _lblScore: UILabel!
    
    //the quiz object
    var quiz = Quiz()
    var quizIndex = 0
    var userScore = 0
    var outOf = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"green")!)
        
        // changes
        _lblCategory.text = quiz.QCategory.rawValue
        outOf = quiz.QList.count
        
        writeScore(score: 0, answered: 0)
        
        //load first question
        assignQuestion()
        
        
        // Do any additional setup after loading the view.
        
        //PLIST STUFF, CURRENTLY UNUSED
        
//        let path = Bundle.main.path(forResource: "questions", ofType: "plist")
//        
//        let dict = NSDictionary(contentsOfFile: path!)
//        
//        let array = dict?.object(forKey: "Quiz1") as! [NSArray]
//        
//        var tempQuiz = Quiz()
//        
//        for qArray in array {
//            var tempQuestion = CoreQuestion() //(title: "UNKNOWN", ans: "UNKNOWN", wrong: ["UNKNOWN"])
//            tempQuestion.QTitle = qArray[0] as! String
//            tempQuestion.QAnswer = qArray[1] as! String
//            tempQuestion.QWAnswer = qArray[2] as! [String]
//            tempQuiz.QList.append(tempQuestion)
//            print(qArray[0])
//        }
//        
//        print(tempQuiz);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSelectedAnswer(_ sender: Any) {
        
        if ((sender as! UIButton).currentTitle == quiz.QList[quizIndex].correctAnswer){
            //tell user they got the question correct, increase score
            userScore = userScore + 1
            if (quizIndex != (outOf - 1)){
                let alert = UIAlertController(title: "+1 Points", message: "That is the correct answer!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        } else{
            //tell user they got the question wrong, do not increase score
            if (quizIndex != (outOf - 1)){
                let alert = UIAlertController(title: "Oops!", message: "That is the wrong answer.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        quizIndex += 1
        writeScore(score: userScore, answered: quizIndex)
        
        //ensure there are more questions to load
        if (quizIndex <= outOf - 1){
            //increase index and load new question
            assignQuestion()
        } else{
            //quiz is done
            let alert = UIAlertController(title: "Finished", message: "You got \(userScore) out of \(outOf)", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Main Menu", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in self.goBack()}))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func assignQuestion(){
        
        _QuestText.text = quiz.QList[quizIndex].text
        
        let assigning = Int(arc4random_uniform(4) + 1)
        
        switch assigning {
        case 1:
            _btnChoice1.setTitle(quiz.QList[quizIndex].correctAnswer, for: .normal)
            _btnChoice2.setTitle(quiz.QList[quizIndex].wrongAnswer1, for: .normal)
            _btnChoice3.setTitle(quiz.QList[quizIndex].wrongAnswer2, for: .normal)
            _btnChoice4.setTitle(quiz.QList[quizIndex].wrongAnswer3, for: .normal)
            break
        case 2:
            _btnChoice1.setTitle(quiz.QList[quizIndex].wrongAnswer3, for: .normal)
            _btnChoice2.setTitle(quiz.QList[quizIndex].wrongAnswer1, for: .normal)
            _btnChoice3.setTitle(quiz.QList[quizIndex].wrongAnswer2, for: .normal)
            _btnChoice4.setTitle(quiz.QList[quizIndex].correctAnswer, for: .normal)
            break
        case 3:
            _btnChoice1.setTitle(quiz.QList[quizIndex].wrongAnswer3, for: .normal)
            _btnChoice2.setTitle(quiz.QList[quizIndex].wrongAnswer2, for: .normal)
            _btnChoice3.setTitle(quiz.QList[quizIndex].correctAnswer, for: .normal)
            _btnChoice4.setTitle(quiz.QList[quizIndex].wrongAnswer1, for: .normal)
            break
        default:
            _btnChoice1.setTitle(quiz.QList[quizIndex].wrongAnswer3, for: .normal)
            _btnChoice2.setTitle(quiz.QList[quizIndex].wrongAnswer1, for: .normal)
            _btnChoice3.setTitle(quiz.QList[quizIndex].correctAnswer, for: .normal)
            _btnChoice4.setTitle(quiz.QList[quizIndex].wrongAnswer2, for: .normal)
        }
    }
    
    func goBack(){
        _ = navigationController?.popToRootViewController(animated: true)
    }

    func writeScore(score: Int, answered: Int){
        _lblScore.text = "Current Score: \(score) / \(answered)"
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
