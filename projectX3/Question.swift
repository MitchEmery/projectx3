//
//  Question.swift
//  projectX3
//
//  Created by Mitchell Emery on 2016-12-07.
//  Copyright © 2016 Mitchell Emery. All rights reserved.
//

import UIKit

class Question: NSObject {
    
    var QTitle: String
    var QAnswer: String
    var QWAnswer: [String]
    
    init(title: String, ans: String, wrong: [String]){
        QTitle = title
        QAnswer = ans
        QWAnswer = wrong
    }
}
