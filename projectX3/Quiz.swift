//
//  Quiz.swift
//  projectX3
//
//  Created by Mitchell Emery on 2016-12-07.
//  Copyright © 2016 Mitchell Emery. All rights reserved.
//

import UIKit
import CoreData


enum Category: String{
    
    case Science
    case Geography
    case Sports
}



class Quiz: NSObject {

    var QName: String
    var QCategory: Category
    var QList: [CoreQuestion]
    
    init(name: String, category : Category, qs : [CoreQuestion]){
        QName = name
        QCategory = category
        QList = qs
    }
        
    override init(){
        self.QName = "ERRR"
        self.QList = []
        self.QCategory = Category.Sports
    }
    
}
