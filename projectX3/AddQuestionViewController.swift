//
//  AddQuestionViewController.swift
//  projectX3
//
//  Created by Neel on 2016-12-09.
//  Copyright © 2016 Mitchell Emery. All rights reserved.
//

import UIKit

class AddQuestionViewController: ViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    var greeting: String = ""
    var pickerDataSource = ["Geography","Science","Sports"]

    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var textInput: UITextField!
    
    @IBOutlet weak var correctInput: UITextField!
    
    @IBOutlet weak var wrongInput1: UITextField!
    
    @IBOutlet weak var wrongInput2: UITextField!
    
    @IBOutlet weak var wrongInput3: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"green")!)

        // Do any additional setup after loading the view.
        
        self.textInput.delegate = self
        self.correctInput.delegate = self
        self.wrongInput1.delegate = self
        self.wrongInput2.delegate = self
        self.wrongInput3.delegate = self
        
        self.picker.dataSource = self
        self.picker.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Methods needed for PickerView
    @available(iOS 2.0, *)
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
    // Event fired when user wants to add question
    @IBAction func addQuestion(_ sender: Any) {
        if (textInput.text == "" || correctInput.text == "" || wrongInput1.text == ""
            || wrongInput2.text == "" || wrongInput3.text == "" ){
            let alert = UIAlertController(title: "Error!", message: "Please fill all fields", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else{
            let appDelegate = (UIApplication.shared.delegate) as! AppDelegate
            
            let context = appDelegate.persistentContainer.viewContext
            
            let question = CoreQuestion(context:context)
            
            question.text = textInput.text
            
            if (picker.selectedRow(inComponent: 0) == 0){
                question.category = "geography"
            }
            if (picker.selectedRow(inComponent: 0) == 1){
                question.category = "science"
            }
            if (picker.selectedRow(inComponent: 0) == 2){
                question.category = "sports"
            }
            
            question.correctAnswer = correctInput.text
            question.wrongAnswer1 = wrongInput1.text
            question.wrongAnswer2 = wrongInput2.text
            question.wrongAnswer3 = wrongInput3.text
            
            appDelegate.saveContext()
            
            let alert = UIAlertController(title: "Added", message: "Question added succesfully!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in self.goBack()}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func goBack(){
        _ = navigationController?.popToRootViewController(animated: true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}
