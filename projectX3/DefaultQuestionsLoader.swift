//
//  DefaultQuestionsLoader.swift
//  projectX3
//
//  Created by Neel on 2016-12-10.
//  Copyright © 2016 Mitchell Emery. All rights reserved.
//

import UIKit

class DefaultQuestionsLoader: NSObject {

    func populateCore(){
        let appDelegate = (UIApplication.shared.delegate) as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let sportsQuestion1 = CoreQuestion(context:context)
        
        sportsQuestion1.text = "Most Blocks shots in NBA history?"
        sportsQuestion1.category = "sports"
        sportsQuestion1.correctAnswer = "Hakeen Olajuwon"
        sportsQuestion1.wrongAnswer1 = "Dikembe Mutombo"
        sportsQuestion1.wrongAnswer2 = "Kareem Abdul-Jabbar"
        sportsQuestion1.wrongAnswer3 = "Mark Eaton"
        
        let sportsQuestion2 = CoreQuestion(context:context)
        
        sportsQuestion2.text = "How many stages were there to the Tour de France in 2009?"
        sportsQuestion2.category = "sports"
        sportsQuestion2.correctAnswer = "21"
        sportsQuestion2.wrongAnswer1 = "30"
        sportsQuestion2.wrongAnswer2 = "24"
        sportsQuestion2.wrongAnswer3 = "19"
        
        let sportsQuestion3 = CoreQuestion(context:context)
        
        sportsQuestion3.text = "Which NFL player is the all time leader in fumbles?"
        sportsQuestion3.category = "sports"
        sportsQuestion3.correctAnswer = "Brett Favre"
        sportsQuestion3.wrongAnswer1 = "Kerry Collins"
        sportsQuestion3.wrongAnswer2 = "Warren Moon"
        sportsQuestion3.wrongAnswer3 = "Dave Krieg"
        
        let sportsQuestion4 = CoreQuestion(context:context)
        
        sportsQuestion4.text = "How many teams are currently in the NFL?"
        sportsQuestion4.category = "sports"
        sportsQuestion4.correctAnswer = "32"
        sportsQuestion4.wrongAnswer1 = "34"
        sportsQuestion4.wrongAnswer2 = "30"
        sportsQuestion4.wrongAnswer3 = "29"
        
        let sportsQuestion5 = CoreQuestion(context:context)
        
        sportsQuestion5.text = "Who is the all time leading scorer in NFL history?"
        sportsQuestion5.category = "sports"
        sportsQuestion5.correctAnswer = "Morten Anderson"
        sportsQuestion5.wrongAnswer1 = "Gary Anderson"
        sportsQuestion5.wrongAnswer2 = "Jason Hansen"
        sportsQuestion5.wrongAnswer3 = "Adam Vinateri"
        
        let sportsQuestion6 = CoreQuestion(context:context)
        
        sportsQuestion6.text = "Who won the 2016 Women's Singles Wimbledon Championship?"
        sportsQuestion6.category = "sports"
        sportsQuestion6.correctAnswer = "Serena Williams"
        sportsQuestion6.wrongAnswer1 = "Simona Halep"
        sportsQuestion6.wrongAnswer2 = "Angelique Kerber"
        sportsQuestion6.wrongAnswer3 = "Andy Murray"
        
        let sportsQuestion7 = CoreQuestion(context:context)
        
        sportsQuestion7.text = "Who won the inaugural edition of the Dubai Open Golf Championship in 2014?"
        sportsQuestion7.category = "sports"
        sportsQuestion7.correctAnswer = "Arjun Atwal"
        sportsQuestion7.wrongAnswer1 = "Wang Jeung-hun"
        sportsQuestion7.wrongAnswer2 = "Simon Yates"
        sportsQuestion7.wrongAnswer3 = "Shiv Kapur"
        
        let sportsQuestion8 = CoreQuestion(context:context)
        
        sportsQuestion8.text = "The 2016 UEFA Championship is being hosted by... ?"
        sportsQuestion8.category = "sports"
        sportsQuestion8.correctAnswer = "France"
        sportsQuestion8.wrongAnswer1 = "Greece"
        sportsQuestion8.wrongAnswer2 = "Italy"
        sportsQuestion8.wrongAnswer3 = "Switzerland"
        
        let sportsQuestion9 = CoreQuestion(context:context)
        
        sportsQuestion9.text = "Which country won the Davis Cup title for the first time in 2014?"
        sportsQuestion9.category = "sports"
        sportsQuestion9.correctAnswer = "Switzerland"
        sportsQuestion9.wrongAnswer1 = "France"
        sportsQuestion9.wrongAnswer2 = "Denmark"
        sportsQuestion9.wrongAnswer3 = "Austria"
        
        let sportsQuestion10 = CoreQuestion(context:context)
        
        sportsQuestion10.text = "Carl Lewis won how many gold medals at the 1984 Olympic games?"
        sportsQuestion10.category = "sports"
        sportsQuestion10.correctAnswer = "Four"
        sportsQuestion10.wrongAnswer1 = "Two"
        sportsQuestion10.wrongAnswer2 = "Eight"
        sportsQuestion10.wrongAnswer3 = "Three"
        
        let geographyQuestion1 = CoreQuestion(context:context)
        
        geographyQuestion1.text = "What city lies on the western shore of the Caspian Sea?"
        geographyQuestion1.category = "geography"
        geographyQuestion1.correctAnswer = "Baku, Azerbaijan"
        geographyQuestion1.wrongAnswer1 = "Tel Aviv, Israel"
        geographyQuestion1.wrongAnswer2 = "Sochi, Russia"
        geographyQuestion1.wrongAnswer3 = "Instanbul, Turkey"
        
        let geographyQuestion2 = CoreQuestion(context:context)
        
        geographyQuestion2.text = "What is the capital of Canada"
        geographyQuestion2.category = "geography"
        geographyQuestion2.correctAnswer = "Ottawa"
        geographyQuestion2.wrongAnswer1 = "Toronto"
        geographyQuestion2.wrongAnswer2 = "Vancouver"
        geographyQuestion2.wrongAnswer3 = "Edmonton"
        
        let geographyQuestion3 = CoreQuestion(context:context)
        
        geographyQuestion3.text = "What country is Jakarta the capital of?"
        geographyQuestion3.category = "geography"
        geographyQuestion3.correctAnswer = "Indonesia"
        geographyQuestion3.wrongAnswer1 = "India"
        geographyQuestion3.wrongAnswer2 = "Jordan"
        geographyQuestion3.wrongAnswer3 = "Brazil"
        
        let geographyQuestion4 = CoreQuestion(context:context)
        
        geographyQuestion4.text = "Which of these countries is the oldest?"
        geographyQuestion4.category = "geography"
        geographyQuestion4.correctAnswer = "Slovakia"
        geographyQuestion4.wrongAnswer1 = "Serbia"
        geographyQuestion4.wrongAnswer2 = "Palau"
        geographyQuestion4.wrongAnswer3 = "Montenegro"
        
        let geographyQuestion5 = CoreQuestion(context:context)
        
        geographyQuestion5.text = "Which island is the largest?"
        geographyQuestion5.category = "geography"
        geographyQuestion5.correctAnswer = "Baffin Island"
        geographyQuestion5.wrongAnswer1 = "Sumatra"
        geographyQuestion5.wrongAnswer2 = "Honshu"
        geographyQuestion5.wrongAnswer3 = "Great Britain"
        
        let geographyQuestion6 = CoreQuestion(context:context)
        
        geographyQuestion6.text = "What is the second deepest location in the world?"
        geographyQuestion6.category = "geography"
        geographyQuestion6.correctAnswer = "Puerto Rico Trench"
        geographyQuestion6.wrongAnswer1 = "Mariana Trench"
        geographyQuestion6.wrongAnswer2 = "Java Trench"
        geographyQuestion6.wrongAnswer3 = "Arctic Basin"
        
        let geographyQuestion7 = CoreQuestion(context:context)
        
        geographyQuestion7.text = "The Kalingrad Oblast borders which body of water?"
        geographyQuestion7.category = "geography"
        geographyQuestion7.correctAnswer = "Baltic Sea"
        geographyQuestion7.wrongAnswer1 = "Mediterranean Sea"
        geographyQuestion7.wrongAnswer2 = "Gulf of Aden"
        geographyQuestion7.wrongAnswer3 = "Gulf of Mexico"
        
        let geographyQuestion8 = CoreQuestion(context:context)
        
        geographyQuestion8.text = "A sand deposit extending into the mouth of a bay is a... ?"
        geographyQuestion8.category = "geography"
        geographyQuestion8.correctAnswer = "Split"
        geographyQuestion8.wrongAnswer1 = "Headland"
        geographyQuestion8.wrongAnswer2 = "Sea Stack"
        geographyQuestion8.wrongAnswer3 = "Berm"
        
        let geographyQuestion9 = CoreQuestion(context:context)
        
        geographyQuestion9.text = "The Earth's core is made up mainly of what?"
        geographyQuestion9.category = "geography"
        geographyQuestion9.correctAnswer = "Iron"
        geographyQuestion9.wrongAnswer1 = "Chromium"
        geographyQuestion9.wrongAnswer2 = "Aluminum"
        geographyQuestion9.wrongAnswer3 = "Silicon"
        
        let geographyQuestion10 = CoreQuestion(context:context)
        
        geographyQuestion10.text = "Ozone holes are more pronouned at the... ?"
        geographyQuestion10.category = "geography"
        geographyQuestion10.correctAnswer = "Poles"
        geographyQuestion10.wrongAnswer1 = "Equator"
        geographyQuestion10.wrongAnswer2 = "Tropic of Cancer"
        geographyQuestion10.wrongAnswer3 = "Tropic of Capricorn"
        
        let scienceQuestion1 = CoreQuestion(context:context)
        
        scienceQuestion1.text = "Which of the following is a non metal that remains liquid at room temperature?"
        scienceQuestion1.category = "science"
        scienceQuestion1.correctAnswer = "Bromine"
        scienceQuestion1.wrongAnswer1 = "Helium"
        scienceQuestion1.wrongAnswer2 = "Magnesium"
        scienceQuestion1.wrongAnswer3 = "Chlorine"
        
        let scienceQuestion2 = CoreQuestion(context:context)
        
        scienceQuestion2.text = "Which type of fire extinguisher is used for petroleum fire?"
        scienceQuestion2.category = "science"
        scienceQuestion2.correctAnswer = "Powder"
        scienceQuestion2.wrongAnswer1 = "Liquid"
        scienceQuestion2.wrongAnswer2 = "Foam"
        scienceQuestion2.wrongAnswer3 = "Soda Acid"
        
        let scienceQuestion3 = CoreQuestion(context:context)
        
        scienceQuestion3.text = "Which among the following is a positvely charged particle emitted by a radioactive element?"
        scienceQuestion3.category = "science"
        scienceQuestion3.correctAnswer = "Beta Ray"
        scienceQuestion3.wrongAnswer1 = "Gamma Ray"
        scienceQuestion3.wrongAnswer2 = "Cathode Ray"
        scienceQuestion3.wrongAnswer3 = "Alpha Ray"
        
        let scienceQuestion4 = CoreQuestion(context:context)
        
        scienceQuestion4.text = "Brass gets discoloured in air because of the presence of which of the following gases in air?"
        scienceQuestion4.category = "science"
        scienceQuestion4.correctAnswer = "Oxygen"
        scienceQuestion4.wrongAnswer1 = "Hydrogen Sulphide"
        scienceQuestion4.wrongAnswer2 = "Carbon Dioxide"
        scienceQuestion4.wrongAnswer3 = "Nitrogen"
        
        let scienceQuestion5 = CoreQuestion(context:context)
        
        scienceQuestion5.text = "Atoms are composed of... ?"
        scienceQuestion5.category = "science"
        scienceQuestion5.correctAnswer = "Electrons and Nuclei"
        scienceQuestion5.wrongAnswer1 = "Electrons"
        scienceQuestion5.wrongAnswer2 = "Protons"
        scienceQuestion5.wrongAnswer3 = "Electrons and Protons"
        
        let scienceQuestion6 = CoreQuestion(context:context)
        
        scienceQuestion6.text = "Epoxy resins are used as... ?"
        scienceQuestion6.category = "science"
        scienceQuestion6.correctAnswer = "Adhesives"
        scienceQuestion6.wrongAnswer1 = "Insecticides"
        scienceQuestion6.wrongAnswer2 = "Moth repellents"
        scienceQuestion6.wrongAnswer3 = "Adhesives"
        
        let scienceQuestion7 = CoreQuestion(context:context)
        
        scienceQuestion7.text = "Isotopes are seperated by... ?"
        scienceQuestion7.category = "science"
        scienceQuestion7.correctAnswer = "Distillation"
        scienceQuestion7.wrongAnswer1 = "Filtration"
        scienceQuestion7.wrongAnswer2 = "Crystallisation"
        scienceQuestion7.wrongAnswer3 = "Sublimation"
        
        let scienceQuestion8 = CoreQuestion(context:context)
        
        scienceQuestion8.text = "Which of the following is used as a moderator in nuclear reactor?"
        scienceQuestion8.category = "science"
        scienceQuestion8.correctAnswer = "Graphite"
        scienceQuestion8.wrongAnswer1 = "Water"
        scienceQuestion8.wrongAnswer2 = "Radium"
        scienceQuestion8.wrongAnswer3 = "Thorium"
        
        let scienceQuestion9 = CoreQuestion(context:context)
        
        scienceQuestion9.text = "Chlorophyll is a naturally occuring chelate compund in which the central metal is... ?"
        scienceQuestion9.category = "science"
        scienceQuestion9.correctAnswer = "Magnesium"
        scienceQuestion9.wrongAnswer1 = "Calcium"
        scienceQuestion9.wrongAnswer2 = "Copper"
        scienceQuestion9.wrongAnswer3 = "Iron"
        
        let scienceQuestion10 = CoreQuestion(context:context)
        
        scienceQuestion10.text = "Which is/are the important raw material(s) required in the cement industry?"
        scienceQuestion10.category = "science"
        scienceQuestion10.correctAnswer = "Limestone"
        scienceQuestion10.wrongAnswer1 = "Clay"
        scienceQuestion10.wrongAnswer2 = "Gypsum and Clay"
        scienceQuestion10.wrongAnswer3 = "Limestome and Clay"
        
        appDelegate.saveContext()
    }
    
}
