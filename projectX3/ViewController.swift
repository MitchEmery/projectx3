//
//  ViewController.swift
//  projectX3
//
//  Created by Mitchell Emery on 2016-12-07.
//  Copyright © 2016 Mitchell Emery. All rights reserved.
//
import WatchConnectivity
import UIKit

class ViewController: UIViewController, WCSessionDelegate {
    
    
    //VAR NEEDED FOR THE WATCH COMMS
    var session: WCSession!
    //
    
    var sportsQ = Quiz(name: "sportsQ", category: Category.Sports, qs:[])
    var scienceQ = Quiz(name: "sceienceQ", category: Category.Science, qs:[])
    var geographyQ = Quiz(name: "geographyQ", category: Category.Geography, qs:[])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        intializeQuizzes();
        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"green")!)
        
        //self.intializeQuizzes()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Used to add new questions to CoreDataModel
        
//        let appDelegate = (UIApplication.shared.delegate) as! AppDelegate
//
//        let context = appDelegate.persistentContainer.viewContext
//
//        let question = CoreQuestion(context:context)
//

//        question.text = "Geo Q"
//        question.category = "geography"
//        question.correctAnswer = "RIGHT"
//        question.wrongAnswer1 = "wrong"
//        question.wrongAnswer2 = "wrong"
//        question.wrongAnswer3 = "wrong"
//       
//
//       
//
//        appDelegate.saveContext()

//        question.text = "Most Blocks shots in NBA history?"
//        question.category = "sports"
//        question.correctAnswer = "Hakeen Olajuwon"
//        question.wrongAnswer1 = "Dikembe Mutombo"
//        question.wrongAnswer2 = "Kareem Abdul-Jabbar"
        
//         question.wrongAnswer3 = "Mark Eaton"
       
//        appDelegate.saveContext()

        isDefaults()
        
        //make sure the watch is supportedr

        
        //WATCH COMMS: PLO
        if (WCSession.isSupported()) {
            session = WCSession.default()
            session.delegate = self;
            session.activate()
        }
        //WATCH COMMS: PLO
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }
    
    //changes
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "play"){
            let QuestionVC: QuestionViewController = segue.destination as! QuestionViewController
            
            let btn = sender as? UIButton
            let selection = btn?.currentTitle
            
            if(selection == "Geography"){
                initializeQuiz(category: "geography")
                //user chose the geography quiz
                QuestionVC.quiz = geographyQ
            }
            if(selection == "Science")
            {
                initializeQuiz(category: "science")
                //user chose the science quiz
                QuestionVC.quiz = scienceQ
            }
            if (selection == "Sports")
            {
                initializeQuiz(category: "sports")
                // user chose the sports quiz
                QuestionVC.quiz = sportsQ
            }
        }
}
    
    
    func intializeQuizzes(){
        
        let appDelegate = (UIApplication.shared.delegate) as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        do{
            let result = try context.fetch(CoreQuestion.fetchRequest())
            
            let questions = result as! [CoreQuestion]
            
            print(questions.count)
            
            // loop through all questions in CoreData and assign them to their respective quizzes
            for question in questions {
                if (question.category == "geography" && geographyQ.QList.count < 10){
                    geographyQ.QList.append(question)
                }
                if (question.category == "sports" && sportsQ.QList.count < 10){
                    sportsQ.QList.append(question)
                }
                if (question.category == "science" && scienceQ.QList.count < 10){
                    scienceQ.QList.append(question)
                }
                
                // used to alter a specifc entry if needed
                
//                if (question.correctAnswer == "Powder"){
//                    question.text = "Which type of fire extinguisher is used for petroleum fire?"
//                    appDelegate.saveContext()
//                }
                
            }
        } catch{
            //PROCESS ERROR
            print("error")
        }
        
        // shuffle questions withing question list
        geographyQ.QList = geographyQ.QList.shuffled();
        sportsQ.QList = sportsQ.QList.shuffled();
        scienceQ.QList = scienceQ.QList.shuffled();
        
    }
    
    func initializeQuiz(category: String){
        
        //get appdelegate
        let appDelegate = (UIApplication.shared.delegate) as! AppDelegate
        
        //set up context for coredata
        let context = appDelegate.persistentContainer.viewContext
        
        //pull coredata
        do{
            let result = try context.fetch(CoreQuestion.fetchRequest())
            
            var questions = result as! [CoreQuestion]
            
            questions = questions.shuffled()
            
            //clear previous quiz questions
            geographyQ.QList = [];
            sportsQ.QList = [];
            scienceQ.QList = [];
            
            // loop through all questions in CoreData and assign them to their respective quizzes
            for question in questions {
                if (question.category == category && category == "geography"
                        && geographyQ.QList.count < 10){
                    geographyQ.QList.append(question)
                }
                if (question.category == category && category == "sports"
                        && sportsQ.QList.count < 10){
                    sportsQ.QList.append(question)
                }
                if (question.category == category && category == "science"
                        && scienceQ.QList.count < 10){
                    scienceQ.QList.append(question)
                }
            }
        } catch{
            //PROCESS ERROR
            print("error")
        }
        
        // shuffle questions within question list
        if (category == "science"){
            scienceQ.QList = scienceQ.QList.shuffled()
        }
        if (category == "geography"){
            geographyQ.QList = geographyQ.QList.shuffled()
        }
        if (category == "sports"){
            sportsQ.QList = sportsQ.QList.shuffled()
        }
        
    }

    /////
    //    THE WATCH COMMUNICATION FUNCTIONS ARE BELOW
    /////
    
    
    
    //methods required for WCSessionDelegate
    func sessionDidDeactivate(_ session: WCSession) {
        //is called when the session becomes in activate
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        //is called when the session becomes active again
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error:Error?)
    {
        //called when the activation is completed
    }
    //The function that sends a broken down quiz to the watch
    func sendQuizToWatch(q: Quiz){
        
        //cant send user defined types
        // break down the quiz into questions, and then take the string componets and add them to the payload
        
        let randomNum:Int = Int(arc4random_uniform(UInt32(q.QList.count))) // range is 0 to list of questions
        
        let quest: CoreQuestion = q.QList[randomNum]
        

        let messageToSend = ["Question": quest.text as Any, "Answer": quest.correctAnswer as Any, "Wrong1":quest.wrongAnswer1 as Any,"Wrong2":quest.wrongAnswer2 as Any,"Wrong3":quest.wrongAnswer3 as Any]
        
        session.sendMessage(messageToSend, replyHandler: { replyMessage in
            //handle the reply
            _ = replyMessage["Value"] as? String
            //we dont care about the reply
        }, errorHandler: {error in
            // catch any errors here
            print(error)
        })

        print("HERE")
        session.sendMessage(messageToSend, replyHandler: nil, errorHandler: {error in print(error)})
    }
    
    
    func isDefaults(){
        
        let appDelegate = (UIApplication.shared.delegate) as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        do{
            let result = try context.fetch(CoreQuestion.fetchRequest())
            
            if (result.count == 0){
                let dq = DefaultQuestionsLoader()
                dq.populateCore()
            }
            
        } catch{
            //PROCESS ERROR
            print("error")
        }

    }

    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void)
    {
        
        //upon recieving a message, send a quiz depending on what the payload of the message recieved is
        if(message["Value"] as? String == "Sports"){
            sendQuizToWatch(q: sportsQ)
        }
        
        if(message["Value"] as? String == "Science"){
            sendQuizToWatch(q: scienceQ)
        }
        
        if(message["Value"] as? String == "Geography"){
            sendQuizToWatch(q: geographyQ)
        }
    }
}

